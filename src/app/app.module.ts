import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DocumentDetailComponent } from './tab1/document-detail/document-detail.component';
import { AddDocumentComponent } from './tab1/add-document/add-document.component';

// 文件上传
import { FileChooser } from '@ionic-native/file-chooser/ngx';
// 文件上传
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { File } from '@ionic-native/file/ngx';
// 文件下载
import { Downloader } from '@ionic-native/downloader/ngx';
// 申请android权限
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
@NgModule({
  declarations: [AppComponent, DocumentDetailComponent, AddDocumentComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    FileChooser,
    FileTransfer,
    File,
    Downloader,
    AndroidPermissions,
    HttpClient,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
