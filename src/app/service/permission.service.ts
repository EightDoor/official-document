import { Injectable } from '@angular/core';
import { TOKEN } from './constant';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class PermissionService {
  constructor(private router: Router) {}
  canActivate() {
    const token = localStorage.getItem(TOKEN);
    if (token) {
      return true;
    } else {
      this.router.navigate(['/login']);
    }
  }
}
