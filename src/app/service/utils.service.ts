import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class UtilsService {
  constructor(private toast: ToastController) {}
  /**
   * 消息弹出框
   * @param message 内容
   * @param position 位置
   */
  async showToast(message: string, position: 'top' | 'middle' | 'bottom') {
    const toast = await this.toast.create({
      message,
      cssClass: 'commonToast',
      position,
      duration: 1500,
    });
    toast.present();
  }
}
