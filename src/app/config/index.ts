const Config = {
  // baseUrl: 'http://gw.start6.cn/jeecg-boot/',
  baseUrl: 'http://gw.start6.cn:8080/jeecg-boot/',
  // 文件静态资源访问地址
  videoUrl: 'http://gw.start6.cn:8080/jeecg-boot/sys/common/static/',
};

export default Config;
