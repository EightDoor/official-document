import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { LoadingComponent } from './loading/loading.component';
import { NoDataComponent } from './no-data/no-data.component';

import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [LoadingComponent, NoDataComponent],
  imports: [CommonModule, ComponentsRoutingModule, IonicModule],
  exports: [LoadingComponent, NoDataComponent],
})
export class ComponentsModule {}
