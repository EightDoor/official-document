import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { IonInfiniteScroll } from '@ionic/angular';
import { RequestService } from '../service/request/request.service';
import { SELECTPEOPLE } from '../service/constant';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  selectValue = 'all';
  list: any[] = [];
  page = 1;
  LoadEvent: any;
  status = 1; // 1加载、2是暂无更多 3是暂无数据 4加载完成
  constructor(private router: Router, private http: RequestService) {}
  // 选择变化的时候
  segmentChanged(val) {
    this.selectValue = val.detail.value;
    console.log(this.selectValue);
    this.getList().then((res) => {});
  }
  // 详情
  GoDetail(item: any) {
    this.router.navigate(['/document-detail'], { queryParams: { id: item.id } });
  }
  // 添加公文
  AddDocument() {
    localStorage.removeItem(SELECTPEOPLE);
    this.router.navigateByUrl('/add-document');
  }
  ngOnInit() {
    this.getList().then((res) => {});
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        if (res.url === '/tabs/tab1') {
          this.getList().then((res) => {});
        }
      }
    });
  }
  // 获取数据
  getList(page = 1, limit = 2000) {
    return new Promise((resolve, reject) => {
      let url = 'gwgl/gwDocument/otherslist';
      if (this.selectValue !== 'all') {
        url = 'gwgl/gwDocument/mylist';
      }
      this.http.get(url, { pageNo: page, pageSize: limit }).subscribe((res) => {
        console.log(res, 'res');
        if (res && res.code === 200) {
          if (this.selectValue === 'all') {
            const data = res.result;
            // if (this.page !== 1) {
            //   if (data.length === 20) {
            //     this.status = 1;
            //     this.list = this.list.concat(data);
            //   } else if (data.length < 20) {
            //     this.status = 2;
            //     this.list = this.list.concat(data);
            //   }
            // } else {

            // }
            this.list = data;
            if (this.list.length === 0) {
              this.status = 3;
            } else {
              this.status = 4;
            }
          } else {
            const data = res.result.records;
            // if (this.page !== 1) {
            //   if (data.length === 20) {
            //     this.status = 1;
            //     this.list = this.list.concat(data);
            //   } else if (data.length < 20) {
            //     this.status = 2;
            //     this.list = this.list.concat(data);
            //   }
            // } else {
            //   this.list = data;
            // }
            this.list = data;
            if (this.list.length === 0) {
              this.status = 3;
            } else {
              this.status = 4;
            }
          }
          resolve();
        } else {
          reject();
        }
      });
    });
  }
  // 下拉刷新
  doRefresh(event) {
    this.page = 1;
    this.getList(1).then((res) => {
      event.target.complete();
      if (this.LoadEvent) {
        this.LoadEvent.complete();
      }
    });
  }
  loadData(event) {
    console.log('Done');
    this.LoadEvent = event.target;
    if (this.status === 1) {
      this.page = this.page + 1;
      this.getList(this.page).then((res) => {
        event.target.complete();
      });
    }
    // 暂无更多数据了
    // if (data.length == 1000) {
    //   event.target.disabled = true;
    // }
  }
}
