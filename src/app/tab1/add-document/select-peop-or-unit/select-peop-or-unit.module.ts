import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { SelectPeopOrUnitRoutingModule } from './select-peop-or-unit-routing.module';
import { SelectPeopOrUnitComponent } from './select-peop-or-unit.component';

@NgModule({
  declarations: [SelectPeopOrUnitComponent],
  imports: [CommonModule, SelectPeopOrUnitRoutingModule, IonicModule, FormsModule],
})
export class SelectPeopOrUnitModule {}
