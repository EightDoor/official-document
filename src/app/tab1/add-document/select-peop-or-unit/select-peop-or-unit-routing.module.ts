import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SelectPeopOrUnitComponent } from './select-peop-or-unit.component';

const routes: Routes = [
  {
    path: '',
    component: SelectPeopOrUnitComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectPeopOrUnitRoutingModule {}
