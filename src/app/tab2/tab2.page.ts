import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { USERINFO } from '../service/constant';
import Config from '../config';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  userInfo: any;
  constructor(private router: Router) {}
  // 退出登录
  LogOut() {
    localStorage.clear();
    this.router.navigateByUrl('/login');
  }
  /**
   * 处理头像
   */
  formatImage() {
    if (this.userInfo && this.userInfo.avatar) {
      return Config.videoUrl + this.userInfo.avatar;
    }
    return '';
  }
  ngOnInit() {
    this.router.events.subscribe((res) => {
      if (res instanceof NavigationEnd) {
        if (res.url === '/tabs/tab2') {
          this.getInfo();
        }
      }
    });
    // 监听路由变化
  }
  getInfo() {
    this.userInfo = localStorage.getItem(USERINFO) ? JSON.parse(localStorage.getItem(USERINFO)) : null;
  }
}
