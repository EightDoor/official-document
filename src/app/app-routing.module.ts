import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ValidationGuard } from './guard/validation/validation.guard';
import { DocumentDetailComponent } from './tab1/document-detail/document-detail.component';
import { AddDocumentComponent } from './tab1/add-document/add-document.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then((m) => m.TabsPageModule),
    canActivate: [ValidationGuard],
  },
  // 添加公文
  {
    path: 'add-document',
    component: AddDocumentComponent,
  },
  // 公文详情
  {
    path: 'document-detail',
    component: DocumentDetailComponent,
    canActivate: [ValidationGuard],
  },
  // 人员选择
  {
    path: 'select-peop',
    loadChildren: () =>
      import('./tab1/add-document/select-peop-or-unit/select-peop-or-unit.module').then(
        (m) => m.SelectPeopOrUnitModule,
      ),
    canActivate: [ValidationGuard],
  },
  // 登录
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
